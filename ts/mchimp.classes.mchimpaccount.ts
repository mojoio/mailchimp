import * as plugins from './mchimp.plugins'
import * as helpers from './mchimp.helpers'
import * as altRequest from 'request'

// interfaces

export class MchimpAccount {
  private _datacenter: string
  private _authBase64: string

  constructor() {
    // nothing here
  }

  /**
   * authenticate
   */
  auth (keyArg: string) {
    this._authBase64 = `apikey ${keyArg}`
    this._datacenter = helpers.getDataCenterFromKey(keyArg)
  }

  /**
   * smartsubscribe to a list
   */
  async smartsubscribe (
    listNameArg: string,
    mailAdressArg: string,
    statusArg: string,
    inputObjectArg: any,
    interestArrayArg: string[]
  ) {
    let list = await this.getListWithName(listNameArg)
    let interestObject: any = {}
    let availableInterests = await this.getInterestsForList(listNameArg)
    for (let interest of interestArrayArg) {
      let validInterest = availableInterests.filter(itemArg => {
        return itemArg.name === interest
      })
      if (validInterest) {
        interestObject[validInterest[0].id] = true
      }
    }
    let response = await this.request('POST',`/lists/${list.id}/members`, {
      email_address: mailAdressArg,
      status: statusArg,
      merge_fields: inputObjectArg,
      interests: interestObject
    })
    console.log(response)
  }

  // gets all available lists on an account
  async getLists () {
    return (await this.request('GET', '/lists')).lists
  }

  async getListWithName (nameArg: string) {
    let lists: any[] = await this.getLists()
    return lists.filter(itemArg => {
      return itemArg.name === nameArg
    })[0]
  }

  async getInterestsForList (listNameArg: string): Promise<any[]> {
    let list = await this.getListWithName(listNameArg)
    let interestsArray = []
    let interestCategories = (await this.request('GET', `/lists/${list.id}/interest-categories`)).categories
    for (let interestCategorie of interestCategories) {
      let interests = (await this.request('GET', `/lists/${list.id}/interest-categories/${interestCategorie.id}/interests`)).interests
      interestsArray = interestsArray.concat(interests)
    }
    return interestsArray
  }

  async request (methodArg, routeArg: string, dataArg = {}) {
    let dataString = JSON.stringify(dataArg, null, ' ')
    // console.log(routeArg)
    // console.log(dataString)
    let optionsArg: plugins.smartrequest.ISmartRequestOptions = {
      method: methodArg,
      headers: {
        'Authorization': this._authBase64,
        'content-type': 'application/json'
      },
      requestBody: dataString
    }
    let requestRoute = `https://${this._datacenter}.api.mailchimp.com/3.0${routeArg}`
    let response: any
    if (methodArg !== 'POST') {
      response = await plugins.smartrequest.request(requestRoute, optionsArg)
      return response.body
    } else {
      altRequest.post({url: requestRoute, body: dataString, headers: {
        'Authorization': this._authBase64,
        'content-type': 'application/json'
      }}, (err, body) => {
        console.log(body)
      })
    }
  }
}

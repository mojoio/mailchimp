"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDataCenterFromKey = (keyArg) => {
    let matchingRegex = /\-(.*)/;
    let datacenter = matchingRegex.exec(keyArg)[1];
    return datacenter;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWNoaW1wLmhlbHBlcnMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi90cy9tY2hpbXAuaGVscGVycy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVXLFFBQUEsb0JBQW9CLEdBQUcsQ0FBQyxNQUFjO0lBQy9DLElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQTtJQUM1QixJQUFJLFVBQVUsR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQzlDLE1BQU0sQ0FBQyxVQUFVLENBQUE7QUFDbkIsQ0FBQyxDQUFBIn0=